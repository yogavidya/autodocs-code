(defpackage :autodocs-code
  (:nicknames :ad-code)
  (:use :cl :cl-interpol)
  (:export :defun+doc))

(named-readtables:in-readtable :interpol-syntax)

(in-package :autodocs-code)

(fiveam:in-suite* ad-code-tests)

;;----------
;; UTILITIES
;;----------

(defun find-value (key l) 
  "Allows to use a flat list as a list of key-value pairs. Returns the member after KEY."
  (second (member key l)))

(fiveam:def-test find-value ()
  (let((l '(a b c d)))
    (fiveam:is-true (eq (find-value 'c l) 'd))))


(defun set-value (key l new-value)   
"Allows to use a flat list as a list of key-value pairs. Sets the member after KEY to NEW-VALUE."
  (setf (second (member key l)) new-value))

(fiveam:def-test set-value ()
  (let((l '(a b c d)))
    (set-value 'c l 'e)
    (fiveam:is-true (eq (find-value 'c l) 'e))))


(defun default-if-null (expr default) (if expr expr default))


;;----------------------------------------------
;; FUNCTION-DATA structure and related functions
;;----------------------------------------------

(defstruct (function-data (:conc-name fd-))
  (args nil :type list)
  (name "" :type symbol)
  (output :function :type symbol)
  (lambda-list nil :type list)  
  (rest nil :type list)
  (body-p nil :type boolean)
  (keywords nil :type list)
  (optionals nil :type list)
  (required nil :type list)
  (description "" :type string)
  (forms nil :type list)
  (docs 
   (list :in nil :out nil :kw nil :opt nil :rest nil) 
   :type list))


(defun fd-initialize (fd)
  "
fd-initialize (function-data) 
Initializes fd-args, fd-forms and fd-description
Step one:
- if fd-args only contains a lambda list, a NIL body is appended
- if one-form body and no documentation, an \"undocumented\" is inserted
Step two:
- if there's a a docstring, the latter is copied to fd-description
- body is copied to fd-forms
- lambda list is copied to fd-lambda-list
REQUIRED:
  * fd (function-data) a function-data structure
RETURNS (function-data): the same structure initialized
"
  (cond ;limit cases: 1) no forms 2) one string constant form
   ((= (length (fd-args fd)) 1)
    (setf (fd-args fd) 
          (append (fd-args fd) (list  nil))))
   ((and (= (length (fd-args fd)) 2) 
         (stringp (second (fd-args fd))))
    (setf (fd-args fd) 
          (list (first (fd-args fd)) 
                "undocumented" 
                (second (fd-args fd))))))
  (if (and (> (length (fd-args fd)) 2) 
           (stringp (second (fd-args fd))))
      (progn (setf (fd-description fd) 
                   (second (fd-args fd)))
        (setf (fd-forms fd) 
              (nthcdr 2 (fd-args fd))))
    (setf (fd-forms fd) 
          (nthcdr 1 (fd-args fd))))
  (setf (fd-lambda-list fd)
        (first (fd-args fd)))
  fd)

(defun fd-scan-lambda-variables (fd)
"
fd-scan-lambda-variables (fd)
- checks if &rest or &body arguments present in lambda list
- initializes fd-body-p and fd-rest accordingly
- if fd-rest, checks that rest or body is the last element in lambda list
  and that there's no &key or &optional
- cuts off &rest or &body from lambda list copy
- checks for &key and copies it to fd-keywords,
  cutting out &optional args if present; removes &key from lambda list copy
- checks for &optional end copies it to fd-optionals, then cuts it off
- copies remaining part of lambda list copy, if any, to fd-required
- returns fd
"
(let* ((ll (copy-tree (fd-lambda-list fd))))
  (setf fd (collect-rest-variable fd))
  (setf fd (check-lambda-list-with-rest fd))
  (when (fd-rest fd)
    (nbutlast ll 2))
  (setf (fd-keywords fd) (rest (member '&key ll)))
  (when (fd-keywords fd)
    (let ((optional-pos (position '&optional (fd-keywords fd))))
      (when optional-pos
        (setf (fd-keywords fd) (subseq (fd-keywords fd) 0 optional-pos))))
    (rplacd (nthcdr (1- (position '&key ll))
                    ll)
            (nthcdr (+ 1 (position '&key ll) 
                       (length (fd-keywords fd))) ll)))
  (setf (fd-optionals fd) (rest  (member '&optional ll)))
  (when (fd-optionals fd) 
    (nbutlast ll (1+ (length (fd-optionals fd)))))
  (setf (fd-required fd) ll)
  fd))

(defun fd-init-lambda-types (fd)
  "(Lambda variables) IN: X OUT: (X :TYPE nil)"
  (dolist 
      (cur-list 
       (list (fd-required fd) (fd-optionals fd) (fd-keywords fd)))
    (init-parameter-list (fd-output fd) cur-list))
  (when (fd-rest fd)
    (setf (fd-rest fd) (list (append (fd-rest fd) (list :type '(cons))))))
  fd)

(defun collect-rest-variable (fd)
  (let* ((rest (find-value '&rest (fd-lambda-list fd)))
         (body (when (and (eq (fd-output fd) :macro)(null rest))
                 (find-value '&body (fd-lambda-list fd)))))
    (assert (null (and rest body)))
    (if body
        (progn
          (setf (fd-rest fd) (list body))
          (setf (fd-body-p fd) T))
      (and rest (progn 
        (setf (fd-rest fd) (list rest))
        (setf (fd-body-p fd) nil))))
    fd))

(defun check-lambda-list-with-rest (fd)
  (when (fd-rest fd)
    (assert 
        (equal (last (fd-lambda-list fd) 2) 
               (if (fd-body-p fd) 
                   `(&body ,(first (fd-rest fd)))
                 `(&rest ,(first (fd-rest fd))))))
    (when (find '&key (fd-lambda-list fd)) 
      (error "Lambda list can't contain both &key and &rest/&body"))
    (when (find '&optional (fd-lambda-list fd)) 
      (error "Lambda list can't contain both &optional and &rest/&body")))
  fd)


(defun default-parameter-info (output p) 
  (if (eq output :method)
      (list (first p) :type (list (second p)))
    (list p :type nil)))
  
(defun init-parameter-list (output pl)
  (loop for i from 0 to (1- (length pl))
        do
        (setf (nth i pl) (default-parameter-info output (nth i pl)))))

(defun store-doc (fd doc)
  (let ((return-value-doc (string-equal (symbol-name (first doc)) "RETURNS")))
    (flet 
        ((store-it(doc docs-section-kw)
           (let* ((section-pos (position docs-section-kw (fd-docs fd)))
                  (section-pos (progn
                                 (assert section-pos)
                                 (1+ section-pos)))
                  (docs-section (nth section-pos (fd-docs fd)))
                  (pos (position-if (lambda(x) 
                                      (eq (first x) (first doc))) docs-section))
                  (cur-doc 
                   (if (eq (fd-output fd) :method)
                       (if (eq :rest docs-section-kw)
                           (list (first doc) 
                                 :type 
                                 (format nil "(CONS ~A)" (first (second doc))) 
                                 :doc (third doc))
                         (list (first doc)
                               :type (list 
                                (second 
                                 (method-lambda-list-parameter fd (first doc))))
                               :doc (third doc)))
                   (list (first doc) 
                         :type (if (eq :rest docs-section-kw) 
                                   (format nil "(CONS ~A)" (first (second doc))) 
                                 (second doc)) 
                         :doc (third doc)))))
             (if pos
                 (setf (nth pos docs-section) cur-doc)
               nil)
             (set-value docs-section-kw (fd-docs fd) docs-section))))
      (cond
       ((and return-value-doc)
        (set-value :out (fd-docs fd) doc))
       ((store-it doc :in) T)
       ((store-it doc :opt) T)
       ((store-it doc :rest) T)
       ((store-it doc :kw) T)
       (T nil)))))
  
(defun extract-doc (fd doc-text)
  (with-input-from-string (s doc-text) 
    (let* ((doc-tag (make-string 3))
           (result (progn
                     (read-sequence doc-tag s :end 3)
                     (list doc-tag
                           (list (read s nil nil) 
                                 (read s nil nil) 
                                 (read-line s nil nil))))))
      (when (and
             (string-equal doc-tag "doc")
             (null (position nil (first (rest result)))))
        (store-doc fd (first (rest result)))))))

(defun fd-parse-docstrings (fd)
  (loop 
   with index = 0
   and count = (length (fd-forms fd))
   finally (return-from fd-parse-docstrings fd)
   for element in (fd-forms fd) do
   (progn
     (if (and (stringp element) (< index (1- count)))
         (progn
           (extract-doc fd element)
           (setf (fd-forms fd) (rest (fd-forms fd)))
           (incf index))
       (loop-finish)))))

(defun method-lambda-list-parameter (fd parameter)
  (find-if 
   (lambda (x)
     (if (atom x)
       (eq x parameter)
       (eq (first x) parameter)))
   (fd-lambda-list fd)))


(defun fd-initialize-docs (fd)
  "Copies lists of argument names to docs according to type"
  (set-value :rest (fd-docs fd) (fd-rest fd))
  (dolist (coords (list (list :in (fd-required fd)) 
                        (list :opt (fd-optionals fd)) 
                        (list :kw (fd-keywords fd))))
    (let ((cur-docs (list)))
      (set-value (first coords) (fd-docs fd)
                 (dolist (cur-parm (second coords) (reverse cur-docs))
                   (push (if (eq (fd-output fd) :method)
			     (let 
                                 ((p 
                                   (method-lambda-list-parameter fd (first cur-parm))))
                               (list 
                                (if (atom p) p (first p)) 
                                :type 
                                (if (atom p) '(list) (second p))
                                :doc nil))
			   (list (first cur-parm)))
			 cur-docs)))))
  fd)

(defun ensure-list (x)
  (if (atom x)
      (list x)
    x))

(defun append-doc-item (fd doc-key item)
  (let ((pos (1+ (position doc-key (fd-docs fd)))))
    (setf (nth pos (fd-docs fd))
          (append (nth pos (fd-docs fd)) (list item))))) 
  
(defun gen-docstring-section (fd title data doc-key)
  (if data
      (format nil "~A:~%~{~A~}"(string-upcase title)
              (let ((items (list)))
                (dolist (cur-item (find-value doc-key (fd-docs fd)) (reverse items))
                  (push
                   (format nil "~T~T* ~A ~A: ~A~%"
                           (first cur-item)
                           (default-if-null 
                            (find-value :type cur-item) "(untyped)")
                           (default-if-null 
                            (find-value :doc cur-item) "(undocumented)" ))
                   items))))
    ""))


(defun gen-docstring (fd)
  (let*  ((ds1 (format nil
                       "~%~A ~A~%  ~A~%"
                      (fd-name fd) 
                      (if (fd-lambda-list fd) (fd-lambda-list fd) "()")
                      (default-if-null 
                       (fd-description fd) "undocumented")))
         (ds2 (gen-docstring-section fd "*required*" (fd-required fd) :in))
         (ds3 (gen-docstring-section fd "optional" (fd-optionals fd) :opt))
         (ds4 (gen-docstring-section fd "*keywords*" (fd-keywords fd) :kw))
         (ds4.5 (gen-docstring-section fd "*rest*" (fd-rest fd) :rest))
         (ds5 (format nil "~%RETURN VALUE (~A): ~A~%"
                      (default-if-null (first (second  (find-value :out (fd-docs fd)))) "untyped")
                      (default-if-null (third  (find-value :out (fd-docs fd))) "undocumented"))))
    (concatenate 'string ds1 ds2 ds3 ds4 ds4.5 ds5)))

(defmacro defun+doc (name &rest defun-data)
  `(def+doc :function ,name ,@defun-data))

(defmacro defmacro+doc (name &rest defun-data)
  `(def+doc :macro ,name ,@defun-data))

(defmacro defmethod+doc (name &rest defun-data)
  `(def+doc :method ,name ,@defun-data))

(defmacro def+doc (output name &rest defun-data)
  "Output: one of (:function :macro :method)"
  (assert (find output '(:function :macro :method)))
  (let ((fd (make-function-data :name name :output output :args defun-data)))
    (fd-initialize fd)
    (fd-scan-lambda-variables fd)
    (fd-init-lambda-types fd)
    (fd-initialize-docs fd) 
    (setq fd (fd-parse-docstrings fd))
    ;(setq forms (read (make-string-input-stream (format nil "~S" forms))))
    `(defun 
            ,(fd-name fd) 
            ,(fd-lambda-list fd) 
       ,(gen-docstring fd) 
       ,@(fd-forms fd))))

(fiveam:run!)

